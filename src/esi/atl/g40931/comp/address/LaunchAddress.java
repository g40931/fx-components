package esi.atl.g40931.comp.address;/**
 * Part of fx-components
 *
 * @author
 */

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class LaunchAddress extends Application {

    Address a;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Test AddressForm");

        VBox root = new VBox();

        AddressForm form1 = new AddressForm();
        AddressForm form2 = new AddressForm();

        Button b = new Button("get()");
        b.setOnAction(e -> {
            a = form1.get();
            form2.set(a); // Breakpoint here lets me check all values
        });
        root.getChildren().addAll(form1, b, form2);

        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.show();
    }


}
