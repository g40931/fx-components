package esi.atl.g40931.comp.address;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.TextField;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

/**
 * Part of fx-components
 *
 * @author Arnaud Solé
 */
public class AddressForm extends Region {
    private final StringProperty title;
    private final StringProperty streetName; // String to hold special ones (12B for example)
    private final StringProperty streetNum;
    private final IntegerProperty cp;
    private final StringProperty localite; // Make this be defined by CP?

    private final TextField titleField;
    private final TextField streetNameField; // String to hold special ones (12B for example)
    private final TextField streetNumField;
    private final TextField cpField;
    private final TextField localiteField; // Make this be defined by CP?

    private final VBox container;

    public AddressForm() {
        title = new SimpleStringProperty("title");
        streetName = new SimpleStringProperty("street name");
        streetNum = new SimpleStringProperty("street num");
        cp = new SimpleIntegerProperty(0);
        localite = new SimpleStringProperty("localite");

        titleField = new TextField();
        streetNameField = new TextField();
        streetNumField = new TextField();
        cpField = new TextField();
        localiteField = new TextField();

        container = new VBox(titleField, streetNameField, streetNumField, cpField, localiteField);

        this.getChildren().add(container);

        this.configureElements();
    }

    public AddressForm(Address from) {
        this();
        title.setValue(from.title);
        streetName.setValue(from.streetName);
        streetNum.setValue(from.streetNum);
        cp.setValue(from.cp);
        localite.setValue(from.localite);
    }

    private void configureElements() {
        cpField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("^\\d+$")) cpField.textProperty().setValue(oldValue);
        });

        titleField.textProperty().bindBidirectional(title);
        streetNameField.textProperty().bindBidirectional(streetName);
        streetNumField.textProperty().bindBidirectional(streetNum);
        cpField.textProperty().bindBidirectional(cp, new StringConverter<Number>() {
            @Override
            public String toString(Number object) {
                return object.toString();
            }

            @Override
            public Number fromString(String string) {
                return Integer.parseInt(string);
            }
        });
        localiteField.textProperty().bindBidirectional(localite);
    }

    public Address get() { // Doesn't check for NULL!
        return new Address(title.get(), streetName.get(), streetNum.get(), cp.get(), localite.get());
    }

    public void set(Address from) {
        title.setValue(from.title);
        streetName.setValue(from.streetName);
        streetNum.setValue(from.streetNum);
        cp.setValue(from.cp);
        localite.setValue(from.localite);
    }
}
