package esi.atl.g40931.comp.address;

/**
 * Part of fx-components
 *
 * @author Arnaud Solé
 */
public class Address {
    public final String title;
    public final String streetName;
    public final String streetNum; // String to hold special ones (12B for example)
    public final int cp;
    public final String localite; // Make this be defined by CP?

    public Address(String title, String streetName, String streetNum, int cp, String localite) {
        this.title = title;
        this.streetName = streetName;
        this.streetNum = streetNum;
        this.cp = cp;
        this.localite = localite;
    }
}
